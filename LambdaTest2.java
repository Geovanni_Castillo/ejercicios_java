public class LambdaTest2{
    public static void main(String[] args){
        //Expresión lambda ==> representa un objeto de una interfaz funcional
        Operaciones op = (num1, num2) -> System.out.println(num1 + num2);

        //op.imprimeSuma(7, 15);
        op.imprimeOperaciones(7, 15);

        LambdaTest2 objeto = new LambdaTest2();
        //objeto.miMetodo(op, 12, 15);
        objeto.miMetodo((num1, num2) -> System.out.println(num1 - num2), 15, 5);

        objeto.miMetodo((num1, num2) -> System.out.println(num1 + num2), 15, 5);
    }

    public void miMetodo(Operaciones op, int num1, int num2){
        //op.imprimeSuma(num1, num2);
        op.imprimeOperaciones(num1, num2);
    }
}